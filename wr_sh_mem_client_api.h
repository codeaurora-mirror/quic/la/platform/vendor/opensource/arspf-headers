/**
 * \file wr_sh_mem_client_api.h
 * \brief
 *  	 This file contains Shared mem Client module APIs
 *
 * Copyright (c) 2019-2020 Qualcomm Innovation Center, Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted (subject to the limitations in the
 * disclaimer below) provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *
 *     * Neither the name of Qualcomm Innovation Center nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE
 * GRANTED BY THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT
 * HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
// clang-format off
/*
$Header: //components/rel/avs.fwk/1.0/api/modules/wr_sh_mem_client_api.h#6 $
*/
// clang-format on

#ifndef WR_SH_MEM_CLEINT_API_H_
#define WR_SH_MEM_CLEINT_API_H_

/*------------------------------------------------------------------------
 * Include files
 * -----------------------------------------------------------------------*/
#include "ar_defs.h"
#include "apm_graph_properties.h"
/**
  @h2xml_title1          {Write Shared Memory Client Module API}
  @h2xml_title_agile_rev {Write Shared Memory Client Module API}
  @h2xml_title_date      {May 23, 2019}
*/

/**
 * Output port ID of the Write Shared Memory Client module
 */
#define PORT_ID_WR_SHARED_MEM_CLIENT_OUTPUT               0x1

/**
 * Input port ID of the Write Shared Memory Client module
 */
#define PORT_ID_WR_SHARED_MEM_CLIENT_INPUT                0x2

/**
 * ID of the Write Shared Memory Client Module
 *
 * This module has one static input port with ID 2 and output port with ID 1
 *
 * This module is supported only in Offload container. The modules functions as the
 * Client to the  MODULE_ID_WR_SHARED_MEM_EP module. The module send the data from the
 * Master process domain to the Write End-point module in the satellite SPF.
 *
 * Supported Input Media Format:
 *    - Any
 */
#define MODULE_ID_WR_SHARED_MEM_CLIENT                    0x0700105C
/**
    @h2xmlm_module         {"MODULE_ID_WR_SHARED_MEM_CLIENT", MODULE_ID_WR_SHARED_MEM_CLIENT}
    @h2xmlm_displayName    {"Write Shared Memory Client"}
    @h2xmlm_description    {
                            This module is used to send data from spf in Master process domain to
                            the spf in satellite process domain using the GPR packet exchange mechanism.
                            This module has one static input port with ID 0 and output port with ID 1
                            }
    @h2xmlm_offloadInsert        { WR_CLIENT }
    @h2xmlm_dataInputPorts       { IN = PORT_ID_WR_SHARED_MEM_CLIENT_INPUT}
    @h2xmlm_dataMaxInputPorts    { 1 }
    @h2xmlm_dataOutputPorts      { OUT = PORT_ID_WR_SHARED_MEM_CLIENT_OUTPUT}
    @h2xmlm_dataMaxOutputPorts   { 1 }
    @h2xmlm_supportedContTypes  { APM_CONTAINER_TYPE_OLC }
    @h2xmlm_isOffloadable        {false}
    @h2xmlm_stackSize            { 4096 }
    @{                     <-- Start of the Module -->
    @}                     <-- End of the Module -->
*/

#endif // WR_SH_MEM_CLIENT_API_H_
